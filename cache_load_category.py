"""
Loading multiple pages automatically in Chrome to make the cache server (first time url opening)
for every protection case category in www.shopgsm.ro website
"""
import webbrowser
from bs4 import BeautifulSoup
from urllib import request
import urllib.request
import re
import timeit

# extracting all the urls from the page
def getLinks(url):

    html_page = request.urlopen(url)
    soup = BeautifulSoup(html_page, features='html.parser')
    links = []
    no_duplicates_links = []
    final_links = []

    # getting all the urls from the requested page which is the argument of the function
    for link in soup.findAll('a', attrs={'href': re.compile("^https://")}):
        links.append(link.get('href'))
    # getting only some particular urls list, not all of them like duplicates or any other non-related urls
    for x in links:
        if x not in no_duplicates_links:
            no_duplicates_links.append(x)
    for y in no_duplicates_links:
        if "https://www.shopgsm.ro/huse/iphone/" in y:
            final_links.append(y)
        elif "https://www.shopgsm.ro/huse/samsung/" in y:
            final_links.append(y)
        elif "https://www.shopgsm.ro/huse/huawei/" in y:
            final_links.append(y)
        elif "https://www.shopgsm.ro/huse/xiaomi/" in y:
            final_links.append(y)
        elif "https://www.shopgsm.ro/folii/xiaomi/" in y:
            final_links.append(y)
        elif "https://www.shopgsm.ro/folii/iphone/" in y:
            final_links.append(y)
        elif "https://www.shopgsm.ro/folii/samsung/" in y:
            final_links.append(y)
        elif "https://www.shopgsm.ro/folii/huawei/" in y:
            final_links.append(y)
    # opening with chrome browser every url from the list
    for link in final_links:
        urllib.request.urlopen(link).read()
        print(link)
    print("The page category has been fully loaded, the number of pages is: \n" + str(len(final_links)))


# calling the function with a timer to show the number of urls for each brand and the runtime program
start = timeit.default_timer()

getLinks("https://shopgsm.ro/huse/iphone.html")
getLinks("https://shopgsm.ro/huse/samsung.html")
getLinks("https://shopgsm.ro/huse/huawei.html")
getLinks("https://shopgsm.ro/huse/xiaomi.html")
getLinks("https://shopgsm.ro/folii/xiaomi.html")
getLinks("https://shopgsm.ro/folii/iphone.html")
getLinks("https://shopgsm.ro/folii/samsung.html")
getLinks("https://shopgsm.ro/folii/huawei.html")
stop = timeit.default_timer()
print('Total loading time program: ', stop - start, "'s")
